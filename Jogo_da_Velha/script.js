// Realiza uma jogada
var vez = true  /* Ao colocar a vez como uma variável booleana, fica mais fácil de controlar o turno apenas negando-a no final. */
var status = 0  /* Essa variável irá nos ajudar a controlar quando encerrar a partida após tanto uma 
                 vitória de um dos lados quanto também houver um empate, tratada no durante do código. */
var velha = 0   /* Essa variável irá auxiliar unica e exclusivamente para quando der empate, tratado no final do código. */
function selecionar(botao) {
    if (botao.innerHTML == "" && status == 0) { /* A condição desse if serve para que um "X" não sobreponha um "O" e vice-versa 
                                                e, com a ajuda da variável status, não conseguir fazer mais nenhuma jogada 
                                                após o encerramento do jogo. */
        if (vez) {
            document.getElementById("IndicadorDaVez").innerHTML = "Agora é a vez da GTi!"
            botao.innerHTML = "X"
        }
        else {
            document.getElementById("IndicadorDaVez").innerHTML = "Cuidado! Ataque da Krysis!"
            botao.innerHTML = "O"
        }
        vez = !vez  /* Ao negar a vez, na próxima jogada será trocada o jogador */
        velha++     /* Ao se somar de 1 em 1, quando velha == 9 teremos duas possibilidades, 
                    ou a Krysis vence ou da empate, o que é resolvido ao final do código. */
    }
    ganhou()    /* Ao chamar a função ganhou ao final da função selecionar,
                 ele verifica se alguém ganhou após o último movimento. */

}

// Zera todos as posições e recomeça o jogo
function resetar() {
    location.reload()   /* Código responsável pelo reset da pág. */
}

// Verifica todos as combinações possíveis de se ganhar o jogo
function ganhou() {
    if (document.getElementsByClassName("casa")[0].innerHTML == document.getElementsByClassName("casa")[1].innerHTML &&
        document.getElementsByClassName("casa")[1].innerHTML == document.getElementsByClassName("casa")[2].innerHTML &&
        (document.getElementsByClassName("casa")[0].innerHTML == "X" ||
            document.getElementsByClassName("casa")[0].innerHTML == "O")) { /* Esse if verifica a possibilidade de vitória nas posições vetoriais 0,1,2 da class casa, independente da marcação. */
        if (document.getElementsByClassName("casa")[0].innerHTML == "O") {  /* Aqui indica o vencedor dependendo da marcação no jogo. */
            document.getElementById("IndicadorVencedor").innerHTML = "A GTi venceu a Krysis! Meus parabéns Astronauta!"
            document.getElementById("IndicadorDaVez").innerHTML = ""    /* Aqui apaga o turno pois acabou o jogo. */
            status = 1  /* Perceba que ao colocar status = 1, ele para de obedecer o primeiro if da função selecionar,
                        assim, eu não poderei adicionar mais "X's" ou "O's".*/
        } else {
            document.getElementById("IndicadorVencedor").innerHTML = "Perdemos gente ;( mas não vamos desistir! Tente mais uma vez!"
            document.getElementById("IndicadorDaVez").innerHTML = ""    /* Aqui apaga o turno pois acabou o jogo. */
            status = 1
        }
    }
    if (document.getElementsByClassName("casa")[0].innerHTML == document.getElementsByClassName("casa")[4].innerHTML &&
        document.getElementsByClassName("casa")[4].innerHTML == document.getElementsByClassName("casa")[8].innerHTML &&
        (document.getElementsByClassName("casa")[0].innerHTML == "X" ||
            document.getElementsByClassName("casa")[0].innerHTML == "O")) { /* Esse if verifica a possibilidade de vitória nas posições vetoriais 0,4,8 da class casa, independente da marcação. */
        if (document.getElementsByClassName("casa")[0].innerHTML == "O") {  /* Aqui indica o vencedor dependendo da marcação no jogo. */
            document.getElementById("IndicadorVencedor").innerHTML = "A GTi venceu a Krysis! Meus parabéns Astronauta!"
            document.getElementById("IndicadorDaVez").innerHTML = ""    /* Aqui apaga o turno pois acabou o jogo. */
            status = 1
        } else {
            document.getElementById("IndicadorVencedor").innerHTML = "Perdemos gente ;( mas não vamos desistir! Tente mais uma vez!"
            document.getElementById("IndicadorDaVez").innerHTML = ""    /* Aqui apaga o turno pois acabou o jogo. */
            status = 1
        }
    }
    if (document.getElementsByClassName("casa")[0].innerHTML == document.getElementsByClassName("casa")[3].innerHTML &&
        document.getElementsByClassName("casa")[3].innerHTML == document.getElementsByClassName("casa")[6].innerHTML &&
        (document.getElementsByClassName("casa")[0].innerHTML == "X" ||
            document.getElementsByClassName("casa")[0].innerHTML == "O")) { /* Esse if verifica a possibilidade de vitória nas posições vetoriais 0,3,6 da class casa, independente da marcação. */
        if (document.getElementsByClassName("casa")[0].innerHTML == "O") {  /* Aqui indica o vencedor dependendo da marcação no jogo. */
            document.getElementById("IndicadorVencedor").innerHTML = "A GTi venceu a Krysis! Meus parabéns Astronauta!"
            document.getElementById("IndicadorDaVez").innerHTML = ""    /* Aqui apaga o turno pois acabou o jogo. */
            status = 1
        } else {
            document.getElementById("IndicadorVencedor").innerHTML = "Perdemos gente ;( mas não vamos desistir! Tente mais uma vez!"
            document.getElementById("IndicadorDaVez").innerHTML = ""    /* Aqui apaga o turno pois acabou o jogo. */
            status = 1
        }
    }
    if (document.getElementsByClassName("casa")[3].innerHTML == document.getElementsByClassName("casa")[4].innerHTML &&
        document.getElementsByClassName("casa")[4].innerHTML == document.getElementsByClassName("casa")[5].innerHTML &&
        (document.getElementsByClassName("casa")[3].innerHTML == "X" ||
            document.getElementsByClassName("casa")[3].innerHTML == "O")) { /* Esse if verifica a possibilidade de vitória nas posições vetoriais 3,4,5 da class casa, independente da marcação. */
        if (document.getElementsByClassName("casa")[3].innerHTML == "O") {  /* Aqui indica o vencedor dependendo da marcação no jogo. */
            document.getElementById("IndicadorVencedor").innerHTML = "A GTi venceu a Krysis! Meus parabéns Astronauta!"
            document.getElementById("IndicadorDaVez").innerHTML = ""    /* Aqui apaga o turno pois acabou o jogo. */
            status = 1
        } else {
            document.getElementById("IndicadorVencedor").innerHTML = "Perdemos gente ;( mas não vamos desistir! Tente mais uma vez!"
            document.getElementById("IndicadorDaVez").innerHTML = ""    /* Aqui apaga o turno pois acabou o jogo. */
            status = 1
        }
    }
    if (document.getElementsByClassName("casa")[1].innerHTML == document.getElementsByClassName("casa")[4].innerHTML &&
        document.getElementsByClassName("casa")[4].innerHTML == document.getElementsByClassName("casa")[7].innerHTML &&
        (document.getElementsByClassName("casa")[1].innerHTML == "X" ||
            document.getElementsByClassName("casa")[1].innerHTML == "O")) { /* Esse if verifica a possibilidade de vitória nas posições vetoriais 1,4,7 da class casa, independente da marcação. */
        if (document.getElementsByClassName("casa")[1].innerHTML == "O") {  /* Aqui indica o vencedor dependendo da marcação no jogo. */
            document.getElementById("IndicadorVencedor").innerHTML = "A GTi venceu a Krysis! Meus parabéns Astronauta!"
            document.getElementById("IndicadorDaVez").innerHTML = ""    /* Aqui apaga o turno pois acabou o jogo. */
            status = 1
        } else {
            document.getElementById("IndicadorVencedor").innerHTML = "Perdemos gente ;( mas não vamos desistir! Tente mais uma vez!"
            document.getElementById("IndicadorDaVez").innerHTML = ""    /* Aqui apaga o turno pois acabou o jogo. */
            status = 1
        }
    }
    if (document.getElementsByClassName("casa")[2].innerHTML == document.getElementsByClassName("casa")[4].innerHTML &&
        document.getElementsByClassName("casa")[4].innerHTML == document.getElementsByClassName("casa")[6].innerHTML &&
        (document.getElementsByClassName("casa")[2].innerHTML == "X" ||
            document.getElementsByClassName("casa")[2].innerHTML == "O")) { /* Esse if verifica a possibilidade de vitória nas posições vetoriais 2,4,6 da class casa, independente da marcação. */
        if (document.getElementsByClassName("casa")[2].innerHTML == "O") {  /* Aqui indica o vencedor dependendo da marcação no jogo. */
            document.getElementById("IndicadorVencedor").innerHTML = "A GTi venceu a Krysis! Meus parabéns Astronauta!"
            document.getElementById("IndicadorDaVez").innerHTML = ""    /* Aqui apaga o turno pois acabou o jogo. */
            status = 1
        } else {
            document.getElementById("IndicadorVencedor").innerHTML = "Perdemos gente ;( mas não vamos desistir! Tente mais uma vez!"
            document.getElementById("IndicadorDaVez").innerHTML = ""    /* Aqui apaga o turno pois acabou o jogo. */
            status = 1
        }
    }
    if (document.getElementsByClassName("casa")[6].innerHTML == document.getElementsByClassName("casa")[7].innerHTML &&
        document.getElementsByClassName("casa")[7].innerHTML == document.getElementsByClassName("casa")[8].innerHTML &&
        (document.getElementsByClassName("casa")[6].innerHTML == "X" ||
            document.getElementsByClassName("casa")[6].innerHTML == "O")) { /* Esse if verifica a possibilidade de vitória nas posições vetoriais 6,7,8 da class casa, independente da marcação. */
        if (document.getElementsByClassName("casa")[6].innerHTML == "O") {  /* Aqui indica o vencedor dependendo da marcação no jogo. */
            document.getElementById("IndicadorVencedor").innerHTML = "A GTi venceu a Krysis! Meus parabéns Astronauta!"
            document.getElementById("IndicadorDaVez").innerHTML = ""    /* Aqui apaga o turno pois acabou o jogo. */
            status = 1
        } else {
            document.getElementById("IndicadorVencedor").innerHTML = "Perdemos gente ;( mas não vamos desistir! Tente mais uma vez!"
            document.getElementById("IndicadorDaVez").innerHTML = ""    /* Aqui apaga o turno pois acabou o jogo. */
            status = 1
        }
    }
    if (document.getElementsByClassName("casa")[2].innerHTML == document.getElementsByClassName("casa")[5].innerHTML &&
        document.getElementsByClassName("casa")[5].innerHTML == document.getElementsByClassName("casa")[8].innerHTML &&
        (document.getElementsByClassName("casa")[2].innerHTML == "X" ||
            document.getElementsByClassName("casa")[2].innerHTML == "O")) { /* Esse if verifica a possibilidade de vitória nas posições vetoriais 2,5,8 da class casa, independente da marcação. */
        if (document.getElementsByClassName("casa")[2].innerHTML == "O") {  /* Aqui indica o vencedor dependendo da marcação no jogo. */
            document.getElementById("IndicadorVencedor").innerHTML = "A GTi venceu a Krysis! Meus parabéns Astronauta!"
            document.getElementById("IndicadorDaVez").innerHTML = ""    /* Aqui apaga o turno pois acabou o jogo. */
            status = 1
        } else {
            document.getElementById("IndicadorVencedor").innerHTML = "Perdemos gente ;( mas não vamos desistir! Tente mais uma vez!"
            document.getElementById("IndicadorDaVez").innerHTML = ""    /* Aqui apaga o turno pois acabou o jogo. */
            status = 1
        }
    }
    if (velha == 9 && status == 0) {    /* Como dito no início, no caso do jogo se decidir na última jogada, haverá duas possibilidades, ou vitória ou empate,
                                        logo, tendo em mente que o status torna-se 1 ao ter uma vitória, basta condicionar esse if para mostrar empate apenas 
                                        para status == 0, assim, solucionando o problema. */
        document.getElementById("IndicadorVencedor").innerHTML = "Empate!"
        document.getElementById("IndicadorDaVez").innerHTML = ""    /* Aqui apaga o turno pois acabou o jogo. */
        status = 1
    }
}