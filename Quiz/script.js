//Nessa variável estão todas as perguntas e respostas que vocês usarão no desafio
var perguntas = [
    {
        texto: 'O céu não é o limite',
        respostas: [
            { valor: 0, texto: 'Ter preguiça' },
            { valor: 1, texto: 'Quando der certo a gente faz' },
            { valor: 3, texto: 'Vamo pra cima familia' },
            { valor: 2, texto: 'Se der for viavel a gente faz' }
        ]
    },
    {
        texto: 'Apaixonados pela missão',
        respostas: [
            { valor: 0, texto: 'Projetos nada mais sao do que numeros' },
            { valor: 1, texto: 'Dinheiro é bom e é isto' },
            { valor: 3, texto: 'Se meu projeto nao ajudar meu cliente entao nao fiz nada' },
            { valor: 2, texto: 'Se eu entender o que esta no codigo entao ta show' }
        ]
    },
    {
        texto: 'Ohana',
        respostas: [
            { valor: 0, texto: 'Ta com problema? Se vira filhao' },
            { valor: 3, texto: 'Voce é meu irmao de batalha, to contigo e nao solto' },
            { valor: 2, texto: 'Voce é da gti, eu tambem entao tudo certo' },
            { valor: 1, texto: 'Fora da Gti nem olho' }
        ]
    },
    {
        texto: 'Integridade',
        respostas: [
            { valor: 0, texto: 'Furar a fila do ru' },
            { valor: 2, texto: 'Farol amarelo pra mim é verde' },
            { valor: 1, texto: 'Clonar cartao de credito' },
            { valor: 3, texto: 'Sou integro' }
        ]
    },
    {
        texto: 'Ser capitão da nave',
        respostas: [
            { valor: 3, texto: 'Eu serei o lider que eu quero pra mim' },
            { valor: 0, texto: 'Eu nao sabia, entao nao fiz' },
            { valor: 1, texto: 'Ve la, depois me conta' },
            { valor: 2, texto: 'To so esperando meu estagio aprovar pra sair da GTi' }
        ]
    },
    {
        texto: 'O que você acha de dar spoiler', /* Minha pergunta :) */
        respostas: [
            { valor: 3, texto: 'Um bom membro não da spoiler' },
            { valor: 0, texto: 'Quero que se foda, paguei o ingresso foi pra comentar depois mesmo' },
            { valor: 1, texto: 'Eu vou comentar baixinho' },
            { valor: 2, texto: 'Mas eu vou comentar só com quem assistiu' }
        ]
    }
]

var perg = 0    /* Seleciona a pergunta na variável pergunta */
var resp = 0    /* Seleciona a resposta na variável pergunta */
var soma = 0    /* Guarda a pontuação do quiz */

function mostrarQuestao() {
    if (perg == -1) {   /* Caso para voltar para a tela inicial após o término do quiz */
        document.getElementById("titulo").innerHTML = "QUIZ DOS VALORES DA GTI"
        document.getElementById("confirmar").innerHTML = "COMEÇAR"
        document.getElementById("listaRespostas").style.display = "none"    /* Retirar os itens da tela pela mudança no CSS */
        document.getElementById("resultado").innerHTML = "" /* Retirar a pontuação da tela inicial */
        perg++
        soma = 0    /* Zera a pontuação para começar novamente */
    } else {
        if (perg == 0) {
            document.getElementById("titulo").innerHTML = perguntas[perg].texto
            for (resp = 0; resp < 4; resp++) {  /* Mostrar todas as respostas dentro da mesma pergunta */
                document.getElementsByTagName("span")[resp].innerHTML = perguntas[perg].respostas[resp].texto
                document.getElementById("listaRespostas").style.display = "inline" /* Mudar o CSS para aparecer as respostas */
                document.getElementsByTagName("input")[resp].checked = false /* Garantir que os itens não comecem marcados */
            }
            document.getElementById("confirmar").innerHTML = "Próxima"
            perg++

        } else {
            if ((perg >= 1 && perg < 6) && check()) {   /* Mostrar as próximas perguntas, garantindo sempre que existe um item marcado */
                document.getElementById("titulo").innerHTML = perguntas[perg].texto
                for (resp = 0; resp < 4; resp++) {
                    document.getElementsByTagName("span")[resp].innerHTML = perguntas[perg].respostas[resp].texto
                    document.getElementsByTagName("input")[resp].checked = false
                }
                perg++
            } else if (perg == 6 && check()) {
                finalizarQuiz()
            }
        }

    }
}

function finalizarQuiz() {  /* Retira as perguntas e as respostas da tela colocando a tela final */
    document.getElementById("titulo").innerHTML = "QUIZ DOS VALORES DA GTI"
    document.getElementById("resultado").innerHTML = "Seu percentual de GTi é " + parseInt((soma / 18) * 100) + "%" /* Mostra a sua pontuação; 
                                                                            o parseInt serve para converter o valor da pontuação para inteiro */
    document.getElementById("listaRespostas").style.display = "none"    /* Retira os itens da tela pela mudança no CSS */
    document.getElementById("confirmar").innerHTML = "Refazer Quiz"
    perg = -1   /* Retorna o quiz para um caso que simula a página inicial */
}

function check() {  /* Função que checa se tem item marcado e soma a pontuação de acordo com o item marcado */
    for (resp = 0; resp < 4; resp++) {
        if (document.getElementsByTagName("input")[resp].checked == true) {
            soma = soma + perguntas[perg - 1].respostas[resp].valor /* Como o caso do zero não checa nada, só a partir do perg == 1, 
                                                                    precisamos captar a soma do perg == 0 e como perg == 6 não soma nada, 
                                                                    perg - 1 engloba todos os valores que devemos somar */
            return true
        }
    }
}